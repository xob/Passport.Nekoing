<?php
namespace passport;

interface IOauthLogin
{
    public function platform();

    public function loadConfig($config);
    
    public function scopeTransform($scope);

    public function authorize($scope, $state = '');
    
    public function parseAuthorizeCode();
    
    /**
     * @param unknown $code
     * @return ExtendAccessToken
     */
    public function token($code);
    
    public function refresh($refreshToken);
    
    /**
     * @param ExtendAccessToken $token
     */
    public function openid($token);
    
    /**
     * @param ExtendAccessToken $token
     * @return ExtendUser
     */
    public function userinfo($token);
}

?>