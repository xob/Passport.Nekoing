<?php
namespace passport;

class ExtendAccessToken
{
    public $data;
    
    public $platform;
    public $token;
    public $expire;
    public $openid;
    public $appid;
    public $refreshToken;
    
    /**
     * @param unknown $data
     * @return \passport\ExtendAccessToken
     */
    public static function build($data)
    {
        $ins = new self();
        
        $ins->platform = $data['platform'];
        $ins->token = $data['token'];
        $ins->expire = $data['expire'];
        $ins->openid = $data['openid'];
        $ins->appid = $data['appid'];
        $ins->refreshToken = $data['refresh_token'];
        return $ins;
    }
}

?>