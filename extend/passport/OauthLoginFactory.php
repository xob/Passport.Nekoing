<?php
namespace passport;

use app\home\model\ExtendLoginConfig;
use nekoing\AppException;
use nekoing\ErrorCode;
class OauthLoginFactory
{
    /**
     * 
     * @param unknown $platform
     * @throws AppException
     * @return \passport\IOauthLogin
     */
    public static function getByPlatform($platform)
    {
        $platform = strtolower($platform);
        $oauthLogin = null;
        switch ($platform) {
            case 'qq':
                $oauthLogin = new QQOauthLogin();
                break;
            case 'baidu':
                $oauthLogin = new BaiduOauthLogin();
                break;
            case 'weixin':
            case 'weibo':
            case 'sina':
            case '163':
        }
        
        if(! $oauthLogin) {
            throw new AppException(ErrorCode::EXTEND_LOGIN_ERROR);
        }
        
        $config = ExtendLoginConfig::get([
            'platform' => $platform
        ]);
        
        if(! $config || $config->status) {
            throw new AppException(ErrorCode::EXTEND_LOGIN_ERROR);
        }
        
        $config = $config->toArray();
        $config['redirect_uri'] = config('oauth.extend_oauth_redirect') . $oauthLogin->platform();
        $oauthLogin->loadConfig($config);
        
        return $oauthLogin;
    }
}

?>