<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    ['user/:openid', 'home/user/info', ['method' => 'get']],
    ['register', 'home/user/register', ['method' => 'get']],
    ['home/login/:platform', 'home/login/third', ['method' => 'get']],
    ['home/jump/:platform', 'home/login/jump', ['method' => 'get']],
];
