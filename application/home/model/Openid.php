<?php
namespace app\home\model;

use think\Model;
use nekoing\Utils;

/**
 * @author Nekoing
 *
 * @property string openid
 * @property string appid
 * @property string uid
 * @property integer create_time
 */
class Openid extends Model
{

    protected $updateTime = false;

    protected $auto = [
        'openid'
    ];

    protected function setOpenidAttr($value)
    {
        if (! $this->isUpdate) {
            return Utils::uniqid32();
        }
        return $value;
    }
    
    public static function getOpenid($uid, $appId)
    {
        $row = self::get([
            'appid' => $appId,
            'uid' => $uid
        ]);
        
        if(! $row) {
            $row = new self();
            $row->uid = $uid;
            $row->appid = $appId;
            $row->save();
        }
        
        return $row->openid;
    }
    
    public function getApp()
    {
        return ThirdApp::get($this->appid);
    }
    
    public function getUser()
    {
        return User::get($this->uid);
    }
}
