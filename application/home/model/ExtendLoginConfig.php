<?php
namespace app\home\model;

use think\Model;

/**
 * @author Nekoing
 *
 * @property integer id
 * @property string platform
 * @property string name
 * @property string appid
 * @property string secret
 */
class ExtendLoginConfig extends Model
{
}
