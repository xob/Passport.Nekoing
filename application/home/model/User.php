<?php
namespace app\home\model;

use think\Model;
use nekoing\Utils;

/**
 * @author Nekoing
 *
 * @property string id
 * @property string username
 * @property string password
 * @property string salt
 * @property string nickname
 * @property string email
 * @property integer sex
 * @property string avatar
 * @property integer status
 * @property integer is_extend_account
 * @property string extend_platform
 * @property string extend_openid
 * @property integer create_time
 * @property integer update_time
 */
class User extends Model
{
    protected $auto = ['id'];
    
    protected function setIdAttr($value)
    {
        if(! $this->isUpdate) {
            return Utils::uniqid16();
        }
        return $value;
    }
    
    protected function setPasswordAttr($value)
    {
        $this->salt = rand(10000, 99999);
        return md5($value . $this->salt);
    }
    
    public function getShowData()
    {
        return [
            'uid' => $this->data['id'],
            'nickname' => $this->data['nickname'],
            'email' => $this->data['email'],
            'sex' => $this->data['sex'],
            'avatar' => $this->data['avatar']
        ];
    }
}
