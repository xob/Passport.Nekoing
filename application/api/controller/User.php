<?php
namespace app\api\controller;

use nekoing\BaseController;
use passport\AccessTokenHelper;
use app\home\model\AccessToken;
class User extends BaseController
{
    public function info()
    {
        AccessTokenHelper::checkAccess(null, AccessToken::SCOPE_BASIC);
        
        $user = AccessTokenHelper::getUser();
        $user = $user->getShowData();
        $user['openid'] = AccessTokenHelper::getAccessToken()->openid;
        $this->ajaxSuccess([
            'user' => $user
        ]);
    }
}